// This sketch takes pin 0 as left encoder input, pin 1 as right encoder input. 
// The radius of the wheel (assuming that both have the same size) and the distance between two wheels
// are to be defined.
// It returns leftWheelSpeed, rightWheelSpeed, orientation, forwardDisplacement and sidewayDisplacement.
// encoder(1) gives leftWheelSpeed
// encoder(2) gives rightWheelSpeed
// encoder(3) gives orientation
// encoder(4) gives forwardDisplacement
// encoder(5) gives sidewayDisplacement


float ans;                             // for return
float leftPulseWidth;                  // in us
float rightPulseWidth;                 // in us
long leftForwardDisplacement = 0;      // in mm
long leftSidewayDisplavement = 0;      // in mm
long rightForwardDisplacement = 0;     // in mm
long rightSidewayDisplavement = 0;     // in mm
long forwardDisplacement = 0;           // in mm
long sidewayDisplacement = 0;       // in mm, left is the positive direction
float orientation = 0;              // in radian, positive when turning left, negative when turning right
float radius =    ;                 // in mm,  define the redius of the wheel here
float d = ;                         //  in mm, define the distance between two wheels
float leftWheelSpeed;               // in mm/s
float rightWheelSpeed;              // in mm/s
float speedDifference;              // in mm/s
float omega;                        //  radian/s, turning angular velocity
bool state;                         // true when going forward, false when going backward

int encoder(int i) {              // put an integer from 1 to 5 to return what you want
  leftPulseWidth = pulseIn(leftEncoder, HIGH);                      // get left encoder time intervals
  rightPulseWidth = pulseIn(rightEncoder, HIGH);                    // get right encoder time intervals

  if (state = true){//if going forward, return the speed to be positive
    leftWheelSpeed = 1000000 * (radius * PI / 15)/leftPulseWidth;   // in mm/s
    rightWheelSpeed = 1000000 * (radius * PI/15)/rightPulseWidth;   // in mm/s
    
  }
  else{             //if backward, return negative speed
    leftWheelSpeed = -1000000 * (radius * PI / 15)/leftPulseWidth;
    rightWheelSpeed = -1000000 * (radius * PI/15)/rightPulseWidth;
  }
  speedDifference = rightWheelSpeed - leftWheelSpeed;
  // get angular velocity for orientation calculation
  omega = speedDifference / d;
  if (speedDifference > 0 && rightPulseWidth > 0){    //if turning left and the right wheel has speed
    orientation += omega * rightPulseWidth;
  }
  else if(speedDifference > 0 && rightPulseWidth = 0){// if turning left but the right wheel stops
    orientation += omega * leftPulseWidth;
  }
  else if(speedDifference < 0 && leftPulseWidth > 0){ // if turning right and the left wheel has speed
    orientation += omega * leftPulseWidth;
  }
  else if(speedDifference < 0 && leftPulseWidth = 0){ // if turning right and the left wheel stops
    orientation += omega * rightPulseWidth;
  }
  else {
    orientation = orientation;                        // else, orientation reamains constant
  }
  orientation = orientation % 2PI;            // prevent 2PI 
  leftForwardDisplacement += leftWheelSpeed * cos(orientation) * leftPulseWidth/1000000; // left forward displacement 
  leftSidewayDisplacement += leftWheelSpeed * sin(orientation) * leftPulseWidth/1000000; // left sideway displacement
  rightForwardDisplacement += rightWheelSpeed * cos(orientation) * rightPulseWidth/1000000; // right forward displacement
  rightSidewayDisplacement += rightWheelSpeed * sin(orientation) * rightPulseWidth/1000000; // right sideway displacement
  forwardDisplacement = (leftForwardDisplacement + rightForwardDisplacement)/2; //overall forward displacement
  sidewayDisplacement = (leftSidewayDisplacement + rightSidewayDisplacement)/2; // overall sideway displacement
  if (i==1){ // 1 returns left speed
    ans = leftWheelSpeed;
  }
  else if(i == 2){ // 2 returns right speed
    ans = rightWheelSpeed;
  }
  else if(i == 3){ // 3 returns orientation
    ans = orientation;
  }
  else if(i ==4){ // 4 returns forward displacement
    ans = forwardDisplacement;
  }
  else if(i == 5){ // 5 returns sideway displacement
  }
  else{
    ans = 0;
  }
  return ans;
}
