#include "All_functions.h"
#include "line_sensor.h"

void setup(){
    pinSetup();
    Serial.begin(9600)

}

void loop(){
    // Test whether the motor working well first
    turn(true, 90);
    turn(false, 90);
    delay(10000); // wait for 10 seconds
    // Check all the three line sensors are working well
    l1 = digitalRead(leftSensor);
    r1 = digitalRead(rightSensor);
    b1 = digitalRead(backSensor);
    // Place the robot on the intersection of the line. 
    // It will turn 360 degrees if they are all good. 
    if (l1 == HIGH && r1 == HIGH){
        turn(true, 360);
    }
    delay(10000);
    // Test the left ultrasonic sensor.
    // Place the dummy at the end of red line and 
    // align the robot before this code works. (distance is 440mm)
    int dist1 = ultrasonicDistanceLeft();
    if (dist1 < 360 && dist1 > 320){ // 460-100(the half width of chassy) and 420-100 
        turn(true, 360);
    }
    delay(10000);
    // Test the forward ultrasonic sensor. 
    // Rotate the robot so that the direction of the forward sensor is in line with dummy.
    int dist2 = ultrasonicDistanceForward();
    if (dist2 < 310 && dist2 > 270) { // 460-150(the half lenght of chassy) and 420-150
        turn(true, 360);
    }
